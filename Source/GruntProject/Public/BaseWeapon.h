// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponProjectileBase.h"
#include "GameFramework/Actor.h"
#include "BaseCharacter.h"
#include "BaseWeapon.generated.h"

UENUM(BlueprintType)
enum class EWeaponFireMode  : uint8
{
	Safe		UMETA(DisplayName = "Safe"),
	SemiAuto	UMETA(DisplayName = "SemiAuto"),
	Burst		UMETA(DisplayName = "Burst"),
	FullAuto	UMETA(DisplayName = "FullAuto"),
};


UCLASS()
class GRUNTPROJECT_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = "Weapon Settings")
	float WeaponDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	int32 MagazineCapacity;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	int32 MagazineAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	TSubclassOf<AWeaponProjectileBase> WeaponProjectileClass = AWeaponProjectileBase::StaticClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	TArray<EWeaponFireMode> WeaponFireModes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	FName WeaponMuzzleSocketName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ABaseCharacter* WeaponOwner;
	
	UFUNCTION(BlueprintNativeEvent)
	void EquipWeapon();

	UFUNCTION(BlueprintNativeEvent)
	void UnEquipWeapon();

	UFUNCTION(BlueprintNativeEvent)
	void ReloadWeapon();

	UFUNCTION(BlueprintNativeEvent)
	void Fire();

	UFUNCTION(BlueprintNativeEvent)
	void CycleFireMode();
	
	UFUNCTION(BlueprintImplementableEvent)
	void OnFireModeChanged();

	UFUNCTION(BlueprintCallable)
	EWeaponFireMode GetCurrentWeaponFireMode() const;
	
	UFUNCTION(BlueprintCallable)
	USkeletalMeshComponent* GetWeaponMeshComponent() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Animtaions")
	UAnimMontage* WeaponFireAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Animtaions")
	UAnimMontage* WeaponDryReloadAnimMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Animtaions")
	UAnimMontage* WeaponTacticalReloadAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Animtaions")
	UAnimMontage* CharacterWeaponDryReloadAnimMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Animtaions")
	UAnimMontage* CharacterWeaponTacticalReloadAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Animtaions")
	UAnimMontage* EquipWeaponAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Animtaions")
	UAnimMontage* UnEquipWeaponAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Sound Effects")
	USoundBase* WeaponFireSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Sound Effects")
	USoundBase* WeaponLowAmmoFireSound;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings| Sound Effects")
	USoundBase* WeaponDryFireSound;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite)
	USkeletalMeshComponent* WeaponMeshComponent;

	UPROPERTY(BlueprintReadWrite)
	EWeaponFireMode CurrentWeaponFireMode;

	int32 FireModeIndex;

	UFUNCTION(BlueprintCallable)
	void SpawnProjectile(TSubclassOf<AWeaponProjectileBase> ProjectileClass, float Damage, FName MuzzleSocket);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	
};
