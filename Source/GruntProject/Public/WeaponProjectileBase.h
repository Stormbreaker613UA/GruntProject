// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Engine/DamageEvents.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "WeaponProjectileBase.generated.h"

UCLASS()
class GRUNTPROJECT_API AWeaponProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponProjectileBase(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "WeaponProjectile")
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditDefaultsOnly, Category = "WeaponProjectile")
	USphereComponent* CollisionComponent;
	UPROPERTY(EditDefaultsOnly, Category = "WeaponProjectile")
	UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(BlueprintReadWrite)
	float WeaponProjectileDamage;
	
	UPROPERTY(EditDefaultsOnly, Category = "WeaponProjectile", Meta = (ClampMin = 0, ForceUnits = "m/sec"))
	float WeaponProjectileVelocity;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetWeaponProjectileDamage(float Damage);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

};
