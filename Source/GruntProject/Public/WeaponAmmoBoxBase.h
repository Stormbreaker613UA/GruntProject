// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseConsumableItem.h"
#include "WeaponAmmoBoxBase.generated.h"

/**
 * 
 */
UCLASS()
class GRUNTPROJECT_API AWeaponAmmoBoxBase : public ABaseConsumableItem
{
	GENERATED_BODY()
public:
	
	AWeaponAmmoBoxBase(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0, ForceUnits = "HP"))
	float WeaponAmmoRestoreAmount;

	virtual void TryToApplyEffectOnCharacter() override;
};
