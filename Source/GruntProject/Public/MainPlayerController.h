// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "PlayerCharacter.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GRUNTPROJECT_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AMainPlayerController(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character",
		Meta = (AllowPrivateAccess, ClampMin = 0, ForceUnits = "deg/s"))
	float LookUpRate{60.0f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character",
		Meta = (AllowPrivateAccess, ClampMin = 0, ForceUnits = "deg/s"))
	float LookRightRate{60.0f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character",
		Meta = (AllowPrivateAccess, ClampMin = 0, ForceUnits = "x"))
	float LookUpMouseSensitivity{2.5f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character",
		Meta = (AllowPrivateAccess, ClampMin = 0, ForceUnits = "x"))
	float LookRightMouseSensitivity{2.5f};

	UFUNCTION(BlueprintCallable)
	UEnhancedInputComponent* GetEnhancedInputComponent() const;

	UFUNCTION(BlueprintCallable)
	APlayerCharacter* GetControlledPlayerCharacter() const;

	/*
	 * Movement input controls
	 */

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	UInputMappingContext* IMC_CharacterMovement;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	int32 CharacterMovementInputPriority = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	UInputAction* LookMouseAction;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	UInputAction* MoveKeyboardAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	UInputAction* LookAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	UInputAction* MoveAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	UInputAction* CrouchAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Character Movement Input", Meta = (AllowPrivateAccess))
	UInputAction* JumpAction;
	
	/*
	 *  Weapon input controls
	 */
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Weapon Input", Meta = (AllowPrivateAccess))
	UInputMappingContext* IMC_Weapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Weapon Input", Meta = (AllowPrivateAccess))
	int32 WeaponInputPriority = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Weapon Input", Meta = (AllowPrivateAccess))
	UInputAction* AimAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Weapon Input", Meta = (AllowPrivateAccess))
	UInputAction* FireAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Weapon Input", Meta = (AllowPrivateAccess))
	UInputAction* ReloadAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings| Weapon Input", Meta = (AllowPrivateAccess))
	UInputAction* CycleFireModeAction;

	UFUNCTION(BlueprintCallable)
	void AddInputMapping(UInputMappingContext* InputMappingContext, int32 Priority);

	UFUNCTION(BlueprintCallable)
	void RemoveInputMapping(UInputMappingContext* InputMappingContext);

protected:

	UPROPERTY(VisibleDefaultsOnly)
	UEnhancedInputComponent* EnhancedInputComponent;

	UPROPERTY(VisibleDefaultsOnly)
	APlayerCharacter* ControlledPlayerCharacter;

	UFUNCTION(BlueprintCallable)
	virtual void  SetupPlayerControllerParameters();
	
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	virtual void OnPossess(APawn* InPawn) override;
	
	virtual void OnUnPossess() override;

		
	/*
	 * Movement controls functions
	 */
	
	UFUNCTION(BlueprintNativeEvent)
	void InputLookMouse(const FInputActionValue& ActionValue);
	
	UFUNCTION(BlueprintNativeEvent)
	void InputLook(const FInputActionValue& ActionValue);
	
	UFUNCTION(BlueprintNativeEvent)
	void InputMoveKeyboard(const FInputActionValue& ActionValue);

	UFUNCTION(BlueprintNativeEvent)
	void InputMove(const FInputActionValue& ActionValue);
		
	UFUNCTION(BlueprintNativeEvent)
	void InputCrouch();

	UFUNCTION(BlueprintNativeEvent)
	void InputJump(const FInputActionValue& ActionValue);
	
	/*
	 *  Weapon controls functions
	 */
		
	UFUNCTION(BlueprintNativeEvent)
	void InputAim(const FInputActionValue& ActionValue);

	UFUNCTION(BlueprintNativeEvent)
	void InputFire(const FInputActionValue& ActionValue);

	UFUNCTION(BlueprintNativeEvent)
	void InputReload(const FInputActionValue& ActionValue);

	UFUNCTION(BlueprintNativeEvent)
	void InputCycleFireMode(const FInputActionValue& ActionValue);
};
