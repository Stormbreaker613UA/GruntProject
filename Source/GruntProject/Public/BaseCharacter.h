// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class ABaseWeapon;

UCLASS()
class GRUNTPROJECT_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	ABaseCharacter(const FObjectInitializer& ObjectInitializer);

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Parameters", Meta = (ClampMin = 0, ForceUnits = "HP"))
	float MaxHealth = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Parameters", Meta = (ClampMin = 0, ForceUnits = "HP"))
	float CurrentHealth = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Parameters", Meta = (ClampMin = 0))
	int32 MaxWeaponAmmo = 360;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Parameters", Meta = (ClampMin = 0))
	int32 CurrentWeaponAmmo = 360;
	
	
	UFUNCTION(BlueprintCallable)
	bool IsDead();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	ABaseWeapon* CurrentEquippedWeapon; 
	
	virtual void BeginPlay() override;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	

public:	

	UFUNCTION(BlueprintCallable)
	ABaseWeapon* GetCurrentEquippedWeapon() const;
	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable)
	void SetMaxHealth(float NewMaxHealth);
	
	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth() const;
	
	UFUNCTION(BlueprintCallable)
	void SetCurrentHealth(float NewCurrentHealth);

	UFUNCTION(BlueprintCallable)
	void SetCurrentWeaponAmmo(int32 Ammo);
	
	UFUNCTION(BlueprintCallable)
	int GetCurrentWeaponAmmo() const;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
