// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "ItemSpawner.generated.h"

UENUM(BlueprintType)
enum class EItemSpawnMethod : uint8
{
	// Spawn item only Once 
	OnlyOnce		UMETA(DisplayName = "Spawn Once"),

	// Spawn item only Once after delay
	AfterDelay		UMETA(DisplayName = "Spawn Once After Delay"),

	// Spawn item Once at the start and respawn item every time after Pickup with a time delay
	AfterPickup		UMETA(DisplayName = "Spawn Item After Pickup") 
};


UCLASS()
class GRUNTPROJECT_API AItemSpawner : public ATargetPoint
{
	GENERATED_BODY()
public:
	
	AItemSpawner(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Spawn Settings")
	TSubclassOf<AActor> ItemToSpawnClass = AActor::StaticClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Spawn Settings")
	EItemSpawnMethod ItemSpawnMethod;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Spawn Settings")
	float SpawnItemTimeDelaySec;
	
protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	AActor* SpawnedItem;
	
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void SpawnItem(TSubclassOf<AActor> SpawnItemClass, EItemSpawnMethod SpawnMethod, float SpawnTimeDelay);//, FTransform ItemTransform);

	FTimerDelegate TimerDelegate;
	FTimerHandle TimerHandle;

	virtual void Tick(float DeltaSeconds) override;
	
};
