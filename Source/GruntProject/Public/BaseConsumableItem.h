// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseCharacter.h"
#include "Components/BoxComponent.h"
#include "BaseConsumableItem.generated.h"

UCLASS()
class GRUNTPROJECT_API ABaseConsumableItem : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ABaseConsumableItem(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* ConsumableItemMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* ConsumableItemCollisionComponent;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
	ABaseCharacter* CurrentOverlappingCharacter;

	virtual void TryToApplyEffectOnCharacter(); // Must be overriden by Children
    
	void OnEffectApplied(); // Call it in the end.

	public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	
};
