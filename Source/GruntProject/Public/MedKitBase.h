// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseConsumableItem.h"
#include "MedKitBase.generated.h"

/**
 * 
 */
UCLASS()
class GRUNTPROJECT_API AMedKitBase : public ABaseConsumableItem
{
	GENERATED_BODY()
public:

	AMedKitBase(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = 0, ForceUnits = "HP"))
	float HealthRestoreAmount; // How much health it could restore

	virtual void TryToApplyEffectOnCharacter() override;
};
