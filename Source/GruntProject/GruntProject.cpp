// Copyright Epic Games, Inc. All Rights Reserved.

#include "GruntProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GruntProject, "GruntProject" );
