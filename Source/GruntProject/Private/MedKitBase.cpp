// Fill out your copyright notice in the Description page of Project Settings.


#include "MedKitBase.h"

AMedKitBase::AMedKitBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void AMedKitBase::TryToApplyEffectOnCharacter()
{
	if (IsValid(CurrentOverlappingCharacter))
	{
		float CurrentCharacterHealth = 	CurrentOverlappingCharacter->GetCurrentHealth();
		float CurrentCharacterMaxHealth = CurrentOverlappingCharacter->GetMaxHealth();

		if((CurrentCharacterMaxHealth < CurrentCharacterMaxHealth) && (CurrentCharacterMaxHealth != 0.f))
		{
			float NewHeath = FMath::Clamp( CurrentCharacterHealth + HealthRestoreAmount,0,CurrentCharacterMaxHealth);
			CurrentOverlappingCharacter->SetCurrentHealth(NewHeath);
			OnEffectApplied();
		}
	}
}
