// Fill out your copyright notice in the Description page of Project Settings.

#include "GruntProject/Public/BaseWeapon.h"

#include "GruntProject/Public/WeaponProjectileBase.h"


// Sets default values
ABaseWeapon::ABaseWeapon(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMeshComponent") );
	SetRootComponent(WeaponMeshComponent);

}

void ABaseWeapon::Fire_Implementation()
{
}

void ABaseWeapon::ReloadWeapon_Implementation()
{
}

void ABaseWeapon::UnEquipWeapon_Implementation()
{
}

void ABaseWeapon::EquipWeapon_Implementation()
{
}

void ABaseWeapon::CycleFireMode_Implementation()
{
	if (WeaponFireModes.Num() > 1)
	{
		if (++FireModeIndex > WeaponFireModes.Num() - 1)
		{
			FireModeIndex = 0;
		}
		CurrentWeaponFireMode = WeaponFireModes[FireModeIndex];
		OnFireModeChanged();
	}
}

EWeaponFireMode ABaseWeapon::GetCurrentWeaponFireMode() const
{
	return CurrentWeaponFireMode;
}

USkeletalMeshComponent* ABaseWeapon::GetWeaponMeshComponent() const
{
	if(IsValid(WeaponMeshComponent))
	{
		return WeaponMeshComponent;
	} else return nullptr;
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	if(IsValid(GetOwner()))
	{
		WeaponOwner = Cast<ABaseCharacter>(GetOwner()); // TODO Delete it in future - Temp solution.
	};
	
}

void ABaseWeapon::SpawnProjectile(TSubclassOf<AWeaponProjectileBase> ProjectileClass, float Damage, FName MuzzleSocket)
{
	FActorSpawnParameters ProjectileSpawnParameters;
	FTransform Muzzle = GetWeaponMeshComponent()->GetSocketTransform(MuzzleSocket);
	AWeaponProjectileBase* Projectile = GetWorld()->SpawnActor<AWeaponProjectileBase>(ProjectileClass, Muzzle, ProjectileSpawnParameters);
	Projectile->SetWeaponProjectileDamage(Damage);
}

// Called every frame
void ABaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

