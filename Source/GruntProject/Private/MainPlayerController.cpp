// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerController.h"
#include "BaseWeapon.h"


AMainPlayerController::AMainPlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

UEnhancedInputComponent* AMainPlayerController::GetEnhancedInputComponent() const
{
	if(IsValid(EnhancedInputComponent))
	{
		return EnhancedInputComponent;
	} return  nullptr;
}

APlayerCharacter* AMainPlayerController::GetControlledPlayerCharacter() const
{
	if(IsValid(ControlledPlayerCharacter))
	{
		return ControlledPlayerCharacter;
	} return nullptr;
}

void AMainPlayerController::AddInputMapping(UInputMappingContext* InputMappingContext, int32 Priority)
{
	auto* EnhancedInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());
	if (IsValid(EnhancedInputSubsystem))
	{
		// if we have it, check if we are not trying to re-add a different priority
		if(!EnhancedInputSubsystem->HasMappingContext(InputMappingContext))
		{
			EnhancedInputSubsystem->AddMappingContext(InputMappingContext, Priority);
		}
	}
}

void AMainPlayerController::RemoveInputMapping(UInputMappingContext* InputMappingContext)
{
	auto* EnhancedInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());
	if (IsValid(EnhancedInputSubsystem))
	{
		if(EnhancedInputSubsystem->HasMappingContext(InputMappingContext))
		{
			EnhancedInputSubsystem->RemoveMappingContext(InputMappingContext);
		}
	}
}

void AMainPlayerController::SetupPlayerControllerParameters()
{
	ControlledPlayerCharacter = Cast<APlayerCharacter>(this->GetCharacter());
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetupPlayerControllerParameters();
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent);

	if(IsValid(EnhancedInputComponent))
	{
		//Movement
		EnhancedInputComponent->BindAction(LookMouseAction, ETriggerEvent::Triggered, this, &ThisClass::InputLookMouse);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ThisClass::InputLook);
		EnhancedInputComponent->BindAction(MoveKeyboardAction, ETriggerEvent::Triggered, this, &ThisClass::InputMoveKeyboard);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ThisClass::InputMove);
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Triggered, this, &ThisClass::InputCrouch);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ThisClass::InputJump);
		//Weapon
		//EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Triggered, this, &ThisClass::InputAim);
		//EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Triggered, this, &ThisClass::InputFire);
		//EnhancedInputComponent->BindAction(ReloadAction, ETriggerEvent::Triggered, this, &ThisClass::InputReload);
		//EnhancedInputComponent->BindAction(CycleFireModeAction, ETriggerEvent::Triggered, this, &ThisClass::InputCycleFireMode);
	}
}

void AMainPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	AddInputMapping(IMC_CharacterMovement, CharacterMovementInputPriority);
	AddInputMapping(IMC_Weapon, WeaponInputPriority);
}

void AMainPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
	RemoveInputMapping(IMC_CharacterMovement);
	RemoveInputMapping(IMC_Weapon);
}

	/*
	* Movement controls functions
	*/
void AMainPlayerController::InputLookMouse_Implementation(const FInputActionValue& ActionValue)
{
	if(IsValid(ControlledPlayerCharacter))
	{
		const auto Value{ActionValue.Get<FVector2D>()};
		
		float PitchInputValue = Value.Y * LookUpMouseSensitivity * -1.f;
		AddPitchInput(PitchInputValue);
		
		float YawInputValue = Value.X * LookRightMouseSensitivity;
		AddYawInput(YawInputValue);
	}
}
void AMainPlayerController::InputLook_Implementation(const FInputActionValue& ActionValue)
{
	if(IsValid(ControlledPlayerCharacter))
	{
		const auto Value{ActionValue.Get<FVector2D>()};

		float PitchInputValue = Value.Y * LookUpRate * GetWorld()->GetDeltaSeconds();
		AddPitchInput(PitchInputValue);
		
		float YawInputValue = Value.X * LookRightRate * GetWorld()->GetDeltaSeconds();
		AddYawInput(YawInputValue);
	}
}
void AMainPlayerController::InputMoveKeyboard_Implementation(const FInputActionValue& ActionValue)
{
	if(IsValid(ControlledPlayerCharacter))
	{
		const auto Value(ActionValue.Get<FVector2D>());
		
		FVector ForwardDirection =  ControlledPlayerCharacter->GetActorForwardVector();
		FVector RightDirection = ControlledPlayerCharacter->GetActorRightVector();
		FVector InputVector = ForwardDirection * Value.Y + RightDirection * Value.X;
		ControlledPlayerCharacter->AddMovementInput(InputVector.GetSafeNormal(), 1);
		
	}
}
void AMainPlayerController::InputMove_Implementation(const FInputActionValue& ActionValue)
{
	if(IsValid(ControlledPlayerCharacter))
	{
		const auto Value(ActionValue.Get<FVector2D>());
				
		FVector ForwardDirection = ControlledPlayerCharacter->GetActorForwardVector();
		FVector RightDirection = ControlledPlayerCharacter->GetActorRightVector();
		FVector InputVector = (ForwardDirection * Value.Y + RightDirection * Value.X ) * 1;
		ControlledPlayerCharacter->AddMovementInput(InputVector);
	}
}
void AMainPlayerController::InputJump_Implementation(const FInputActionValue& ActionValue)
{
	if(IsValid(ControlledPlayerCharacter))
	{
		ControlledPlayerCharacter->Jump();
	}
}
void AMainPlayerController::InputCrouch_Implementation()
{
	if(IsValid(ControlledPlayerCharacter))
	{
		if(!ControlledPlayerCharacter->bIsCrouched)
		{
			ControlledPlayerCharacter->Crouch(false);
		}
		else
		{
			ControlledPlayerCharacter->UnCrouch(false); 
		}
	}
}

	/*
	 *  Weapon controls functions
	 */
void AMainPlayerController::InputAim_Implementation(const FInputActionValue& ActionValue)
{
	//To be done in future or will be deleted.
}

void AMainPlayerController::InputFire_Implementation(const FInputActionValue& ActionValue)
{
	GetControlledPlayerCharacter()->GetCurrentEquippedWeapon()->Fire();
}

void AMainPlayerController::InputReload_Implementation(const FInputActionValue& ActionValue)
{
	GetControlledPlayerCharacter()->GetCurrentEquippedWeapon()->ReloadWeapon();
}

void AMainPlayerController::InputCycleFireMode_Implementation(const FInputActionValue& ActionValue)
{
	GetControlledPlayerCharacter()->GetCurrentEquippedWeapon()->CycleFireMode();
}
