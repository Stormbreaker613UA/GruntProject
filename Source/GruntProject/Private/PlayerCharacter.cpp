// Fill out your copyright notice in the Description page of Project Settings.


#include "GruntProject/Public/PlayerCharacter.h"



APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("Player Camera");
	CameraComponent->SetupAttachment(GetMesh(), "head");
}


