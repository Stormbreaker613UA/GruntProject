// Fill out your copyright notice in the Description page of Project Settings.

#include "GruntProject/Public/ItemSpawner.h"


AItemSpawner::AItemSpawner(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SpawnItemTimeDelaySec = 5.0f;
	ItemSpawnMethod = EItemSpawnMethod::OnlyOnce;
}

void AItemSpawner::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnItem(ItemToSpawnClass, ItemSpawnMethod, SpawnItemTimeDelaySec);
	
}

void AItemSpawner::SpawnItem(TSubclassOf<AActor> SpawnItemClass, EItemSpawnMethod SpawnMethod, float SpawnTimeDelay)
{
	if(IsValid(SpawnItemClass))
	{
		if(SpawnMethod == EItemSpawnMethod::OnlyOnce)
		{
			SpawnedItem = GetWorld()->SpawnActor<AActor>(ItemToSpawnClass, GetActorTransform());
		}

		if(SpawnMethod == EItemSpawnMethod::AfterDelay)
		{
			TimerDelegate.BindLambda([&]
			{	
				SpawnedItem = GetWorld()->SpawnActor<AActor>(ItemToSpawnClass, GetActorTransform());
			});
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, SpawnTimeDelay, false);
		}

		if(SpawnMethod == EItemSpawnMethod::AfterPickup)
		{
			SpawnedItem = GetWorld()->SpawnActor<AActor>(ItemToSpawnClass, GetActorTransform());

			TimerDelegate.BindLambda([&]
			{
				if(!IsValid(SpawnedItem))
				{
					SpawnedItem = GetWorld()->SpawnActor<AActor>(ItemToSpawnClass, GetActorTransform()); // Inf loop
				}
			});
			
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, SpawnTimeDelay, true);
		}
	}
}


void AItemSpawner::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}
