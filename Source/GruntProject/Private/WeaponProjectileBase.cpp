// Fill out your copyright notice in the Description page of Project Settings.

#include "GruntProject/Public/WeaponProjectileBase.h"


// Sets default values
AWeaponProjectileBase::AWeaponProjectileBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	CollisionComponent->InitSphereRadius(15.0f);
	SetRootComponent(CollisionComponent);
	CollisionComponent->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComponent->OnComponentHit.AddDynamic(this, &AWeaponProjectileBase::OnHit);
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	Mesh->SetupAttachment(CollisionComponent);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = false;
	//Disable auto activation in order to set custom velocity specified by the user in blueprint.
	ProjectileMovementComponent->bAutoActivate = false;

	//After 10 seconds the bullet is destroyed
	InitialLifeSpan = 10.0f;
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//WeaponProjectileVelocity is our velocity in meters per second
	WeaponProjectileVelocity = 910.0f;

	//Set our inital velocity to 0
	ProjectileMovementComponent->InitialSpeed = 0.0f;
	ProjectileMovementComponent->MaxSpeed = WeaponProjectileVelocity * 100.f;

}

// Called when the game starts or when spawned
void AWeaponProjectileBase::BeginPlay()
{
	Super::BeginPlay();

	//Setup our velocity to be in cm/s and set our last location to the spawn location
	WeaponProjectileVelocity *= 100.f;
	//Calculate and set the velocity based on the velocity set by the user, then activate the projectile movement.
	ProjectileMovementComponent->Velocity = GetActorForwardVector() * WeaponProjectileVelocity;
	ProjectileMovementComponent->Activate();
	
}

// Called every frame
void AWeaponProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponProjectileBase::SetWeaponProjectileDamage(float Damage)
{
	WeaponProjectileDamage = Damage;
}

void AWeaponProjectileBase::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                  FVector NormalImpulse, const FHitResult& Hit)
{
	if (IsValid(OtherActor))
	{				
		OtherActor->TakeDamage(WeaponProjectileDamage,FDamageEvent(),GetInstigatorController(),this);
	}
	
}
