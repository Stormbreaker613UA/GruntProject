// Fill out your copyright notice in the Description page of Project Settings.


#include "GruntProject/Public/BaseCharacter.h"

#include "GruntProject/Public/BaseWeapon.h"

// Sets default values
ABaseCharacter::ABaseCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CurrentHealth = MaxHealth;
}

bool ABaseCharacter::IsDead()
{
	if (CurrentHealth <= 0)
		return true;
	else return  false;
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

float ABaseCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	CurrentHealth -= DamageAmount;
	if(IsDead())
	{
		Destroy();
	}
	
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}


ABaseWeapon* ABaseCharacter::GetCurrentEquippedWeapon() const
{
	if(IsValid(CurrentEquippedWeapon))
	{
		return CurrentEquippedWeapon;
	} return nullptr;
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float ABaseCharacter::GetMaxHealth() const
{
	return MaxHealth;
}

void ABaseCharacter::SetMaxHealth(float NewMaxHealth)
{
	MaxHealth = NewMaxHealth;
}

float ABaseCharacter::GetCurrentHealth() const
{
	return CurrentHealth;
}

void ABaseCharacter::SetCurrentHealth(float NewCurrentHealth)
{
	CurrentHealth = NewCurrentHealth;
}

void ABaseCharacter::SetCurrentWeaponAmmo(int Ammo)
{
	CurrentWeaponAmmo = Ammo;
}

int ABaseCharacter::GetCurrentWeaponAmmo() const
{
	return CurrentWeaponAmmo;
}


// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}
