// Fill out your copyright notice in the Description page of Project Settings.


#include "GruntProject/Public/BaseConsumableItem.h"

// Sets default values
ABaseConsumableItem::ABaseConsumableItem(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ConsumableItemCollisionComponent = CreateDefaultSubobject<UBoxComponent>("Consumable Item Collision Component");
	SetRootComponent(ConsumableItemCollisionComponent);

	ConsumableItemMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Consumable Item Mesh Component");
	ConsumableItemMeshComponent->SetupAttachment(ConsumableItemCollisionComponent);


	ConsumableItemCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &ABaseConsumableItem::OnOverlapBegin);

}

// Called when the game starts or when spawned
void ABaseConsumableItem::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseConsumableItem::TryToApplyEffectOnCharacter()
{
}

void ABaseConsumableItem::OnEffectApplied()
{
	CurrentOverlappingCharacter = nullptr;
	Destroy();
}

// Called every frame
void ABaseConsumableItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseConsumableItem::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->IsA(ABaseCharacter::StaticClass()))
	{
		CurrentOverlappingCharacter = Cast<ABaseCharacter>(OtherActor);
		
		if (IsValid(CurrentOverlappingCharacter))
		{
			TryToApplyEffectOnCharacter(); // TODO Problem could be here
		}
	}
}

