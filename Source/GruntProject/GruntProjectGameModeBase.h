// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GruntProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GRUNTPROJECT_API AGruntProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
