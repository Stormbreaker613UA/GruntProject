﻿using UnrealBuildTool;

public class TrueFPSAnimSystemRuntime : ModuleRules
{
    public TrueFPSAnimSystemRuntime(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core", "GameplayTags", "AnimationCore", "AnimGraphRuntime", "AnimGraph"
            }
        );
        
        PublicDependencyModuleNames.AddRange(
            new string[] { 
                "Core", 
                "CoreUObject", 
                "Engine", 
                "Slate",
                "AnimGraphRuntime",
                "BlueprintGraph",
            }
        );

        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "CoreUObject",
                "Engine",
                "Slate",
                "InputCore",
                "SlateCore",
                "EditorFramework",
                "UnrealEd",
                "GraphEditor",
                "PropertyEditor",
                "EditorStyle",
                "ContentBrowser",
                "KismetWidgets",
                "ToolMenus",
                "KismetCompiler",
                "Kismet",
                "EditorWidgets",
                "UnrealEd",
                "GraphEditor",
            }
        );
        
        PrivateIncludePathModuleNames.AddRange(
            new string[] {
                "Persona",
                "SkeletonEditor",
                "AdvancedPreviewScene",
                "AnimationBlueprintEditor",
            }
        );
        
    }
}