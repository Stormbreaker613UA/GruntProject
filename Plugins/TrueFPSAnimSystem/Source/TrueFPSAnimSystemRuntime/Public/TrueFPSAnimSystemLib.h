﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TrueFPSAnimInstanceState.h"
#include "GameFramework/Character.h"
#include "KismetAnimationLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/KismetMathLibrary.h"
#include "TrueFPSAnimSystemLib.generated.h"

/**
 * 
 */
UCLASS()
class TRUEFPSANIMSYSTEMRUNTIME_API UTrueFPSAnimSystemLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable)
	void RefreshAimRotation(float DeltaTime, float NonLocalCameraRotationInterpSpeed, float CharacterLeanValue, FRotator CurrentControllerInputValues, ACharacter* PlayerCharacter, FTrueFPSAnimInstanceState& AnimInstanceState);

	UFUNCTION(BlueprintCallable)
	void RefreshLocomotionState(float DeltaTime, ACharacter* PlayerCharacter, bool bIsRunning, float CrouchValue, FTrueFPSAnimInstanceState& AnimInstanceState);
	
	UFUNCTION(BlueprintCallable) // <- I have problems with this
	void RefreshRelativeTransforms(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void RefreshAiming(float DeltaTime, FTrueFPSAnimInstanceState& AnimInstanceState, bool bIsAiming, float AimingInterpSpeed);

	UFUNCTION(BlueprintCallable) // <- I have problems with this
	void RefreshAccumulativeOffsets(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void RefreshPlacementTransform(float DeltaTime, FTrueFPSAnimInstanceState& AnimInstanceState, float WeaponPitchTiltMultiplier);

	UFUNCTION(BlueprintCallable)
	void RefreshTurnInPlaceState(float DeltaTime, FTrueFPSAnimInstanceState& AnimInstanceState, ACharacter* PlayerCharacter);

	UFUNCTION(BlueprintCallable)
	void PostRefresh(FTrueFPSAnimInstanceState& AnimInstanceState, ACharacter* PlayerCharacter);

	
};
