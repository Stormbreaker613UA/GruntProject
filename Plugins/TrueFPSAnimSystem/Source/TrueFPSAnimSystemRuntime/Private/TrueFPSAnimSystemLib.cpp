﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TrueFPSAnimSystemLib.h"



void UTrueFPSAnimSystemLib::RefreshAimRotation(float DeltaTime, float NonLocalCameraRotationInterpSpeed, float CharacterLeanValue, FRotator CurrentControllerInputValues, ACharacter* PlayerCharacter, FTrueFPSAnimInstanceState& AnimInstanceState)
{
	check(IsInGameThread());

	//Check if PlayerCharacter is NOT Null
	if(!IsValid(PlayerCharacter))
	{
		UE_LOG(LogTemp, Error, TEXT("PlayerCharacter is NULL!"));
		return;
	}
	
	const APlayerController* PlayerController = PlayerCharacter->GetController<APlayerController>();
	
	if (IsValid(PlayerController))
	{
		const FRotator AddRot(
			CurrentControllerInputValues.Pitch * PlayerController->InputPitchScale_DEPRECATED,
			CurrentControllerInputValues.Yaw * PlayerController->InputYawScale_DEPRECATED,
			CurrentControllerInputValues.Roll * PlayerController->InputRollScale_DEPRECATED);

		const FRotator PreCalculatedCameraRot = PlayerCharacter->GetBaseAimRotation() + AddRot;

		constexpr float ClampAngle = 89.f;
		AnimInstanceState.CameraRotation = FRotator(FMath::ClampAngle(PreCalculatedCameraRot.Pitch, -ClampAngle, ClampAngle), PreCalculatedCameraRot.Yaw, PreCalculatedCameraRot.Roll);
	}
	else
	{
		constexpr float ClampAngle = 89.f;
		FRotator ClampedBaseAimRotation = PlayerController->GetPawn()->GetBaseAimRotation();
		ClampedBaseAimRotation.Pitch = FMath::ClampAngle(ClampedBaseAimRotation.Pitch, -ClampAngle, ClampAngle);

		AnimInstanceState.CameraRotation = UKismetMathLibrary::RInterpTo(AnimInstanceState.CameraRotation, ClampedBaseAimRotation, DeltaTime, NonLocalCameraRotationInterpSpeed);
	}
	
	AnimInstanceState.AimRotation = AnimInstanceState.CameraRotation - PlayerCharacter->GetActorRotation();

	// Leaning
	AnimInstanceState.AimRotation.Roll = CharacterLeanValue;
	
}

void UTrueFPSAnimSystemLib::RefreshLocomotionState(float DeltaTime, ACharacter* PlayerCharacter, bool bIsRunning,
	float CrouchValue, FTrueFPSAnimInstanceState& AnimInstanceState)
{
	check(IsInGameThread());
	if(!IsValid(PlayerCharacter))
	{
		UE_LOG(LogTemp, Error, TEXT("PlayerCharacter is NULL!"));
		return;
	}
	
	AnimInstanceState.MovementDirection = UKismetAnimationLibrary::CalculateDirection(PlayerCharacter->GetCharacterMovement()->Velocity, FRotator(0.f, PlayerCharacter->GetBaseAimRotation().Yaw, 0.f));
	AnimInstanceState.MovementSpeed = PlayerCharacter->GetCharacterMovement()->Velocity.Size();

	AnimInstanceState.bIsFalling = PlayerCharacter->GetCharacterMovement()->IsFalling();
	AnimInstanceState.bIsRunning = bIsRunning;
	AnimInstanceState.CrouchValue = CrouchValue;
}

void UTrueFPSAnimSystemLib::RefreshAiming(float DeltaTime, FTrueFPSAnimInstanceState& AnimInstanceState,
	bool bIsAiming, float AimingInterpSpeed)
{
	check(IsInGameThread());
	AnimInstanceState.AimingValue = UKismetMathLibrary::FInterpTo(AnimInstanceState.AimingValue, bIsAiming ? 1.f : 0.f, DeltaTime, AimingInterpSpeed);
}

void UTrueFPSAnimSystemLib::RefreshPlacementTransform(float DeltaTime, FTrueFPSAnimInstanceState& AnimInstanceState, float WeaponPitchTiltMultiplier)
{
	FVector TargetPlacementLocation{FVector::ZeroVector};
	FRotator TargetPlacementRotation{FRotator::ZeroRotator};

	TargetPlacementRotation.Pitch = AnimInstanceState.CameraRotation.Pitch * WeaponPitchTiltMultiplier;
	TargetPlacementLocation.Z = -AnimInstanceState.CameraRotation.Pitch * (WeaponPitchTiltMultiplier / 3.f);
	
	AnimInstanceState.PlacementTransform = AnimInstanceState.CurrentWeaponCustomOffsetTransform * FTransform(TargetPlacementRotation, TargetPlacementLocation);
}

void UTrueFPSAnimSystemLib::RefreshTurnInPlaceState(float DeltaTime, FTrueFPSAnimInstanceState& AnimInstanceState, ACharacter* PlayerCharacter)
{
	if(!IsValid(PlayerCharacter))
	{
		UE_LOG(LogTemp, Error, TEXT("PlayerCharacter is NULL!"));
		return;
	}
	
	if (!AnimInstanceState.bIsTurningInPlace && abs(AnimInstanceState.RootYawOffset) < 2.f && PlayerCharacter->GetCharacterMovement()->Velocity.Size() < AnimInstanceState.StationaryVelocityThreshold)
	{
		AnimInstanceState.bIsTurningInPlace = true;
	}

	if (AnimInstanceState.bIsTurningInPlace)
	{
		AnimInstanceState.RootYawOffset += FRotator::NormalizeAxis(AnimInstanceState.LastCameraRotation.Yaw - AnimInstanceState.CameraRotation.Yaw);

		// If exceeded rotation or velocity thresholds, set turn in place to false and set rot speed to desired speed
		if (PlayerCharacter->GetCharacterMovement()->Velocity.Size() >= AnimInstanceState.StationaryVelocityThreshold)
		{
			AnimInstanceState.bIsTurningInPlace = false;
			AnimInstanceState.StationaryYawInterpSpeed = 8.f;
		}
		else if (abs(AnimInstanceState.RootYawOffset) >= AnimInstanceState.StationaryYawThreshold)
		{
			AnimInstanceState.bIsTurningInPlace = false;
			AnimInstanceState.StationaryYawInterpSpeed = 5.f;
		}

		// If no longer turning in place, set the rotation amount for turning animation usage
		if (!AnimInstanceState.bIsTurningInPlace)
		{
			AnimInstanceState.StationaryYawAmount = -AnimInstanceState.RootYawOffset;
		}
	}

	if (!AnimInstanceState.bIsTurningInPlace && AnimInstanceState.RootYawOffset)
	{
		const float YawDifference = FRotator::NormalizeAxis(AnimInstanceState.LastCameraRotation.Yaw - AnimInstanceState.CameraRotation.Yaw);
		AnimInstanceState.RootYawOffset += YawDifference;
		
		if (-YawDifference > 0.f == AnimInstanceState.StationaryYawAmount > 0.f)
		{
			AnimInstanceState.StationaryYawAmount += -YawDifference;
		}

		AnimInstanceState.StationaryYawSpeedNormal = FMath::Clamp<float>(AnimInstanceState.StationaryYawAmount / 180.f, 1.5f, 3.f);
            
		// Never allow the yaw offset to exceed the yaw threshold
		AnimInstanceState.RootYawOffset = FMath::ClampAngle(AnimInstanceState.RootYawOffset, -AnimInstanceState.StationaryYawThreshold, AnimInstanceState.StationaryYawThreshold);
            
		// Never allow yaw offset to exceed yaw threshold
		AnimInstanceState.RootYawOffset = UKismetMathLibrary::FInterpTo(AnimInstanceState.RootYawOffset, 0.f, DeltaTime, AnimInstanceState.StationaryYawInterpSpeed);

		// Once matched rotation, clear vars
		if (abs(AnimInstanceState.RootYawOffset) < 2.f)
		{
			AnimInstanceState.StationaryYawInterpSpeed = 0.f;
			AnimInstanceState.StationaryYawAmount = 0.f;
			AnimInstanceState.RootYawOffset = 0.f;
		}
	}
}

void UTrueFPSAnimSystemLib::PostRefresh(FTrueFPSAnimInstanceState& AnimInstanceState, ACharacter* PlayerCharacter)
{
	if(!IsValid(PlayerCharacter))
	{
		UE_LOG(LogTemp, Error, TEXT("PlayerCharacter is NULL!"));
		return;
	}
	
	AnimInstanceState.LastCameraRotation = AnimInstanceState.CameraRotation;
	AnimInstanceState.LastVelocity = PlayerCharacter->GetCharacterMovement()->Velocity;
}


