﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNode_FPSArmsIK.h"
#include "Animation/AnimInstanceProxy.h"
#include "AnimationCore/Public/TwoBoneIK.h"

FAnimNode_FPSArmsIK::FAnimNode_FPSArmsIK()
{
	RightHand = FBoneReference(FName("hand_r"));
	LeftHand = FBoneReference(FName("hand_l"));
	
}

void FAnimNode_FPSArmsIK::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	BasePose.Initialize(Context);

	const FBoneContainer& RequiredBones = Context.AnimInstanceProxy->GetRequiredBones();
	RightHand.Initialize(RequiredBones);
	LeftHand.Initialize(RequiredBones);
	
	const FReferenceSkeleton& RefSkel = RequiredBones.GetReferenceSkeleton();
	
	RightLowerArmIndex = RefSkel.GetParentIndex(RightHand.BoneIndex);
	if(RightLowerArmIndex != INDEX_NONE) RightUpperArmIndex = RefSkel.GetParentIndex(RightLowerArmIndex);

	LeftLowerArmIndex = RefSkel.GetParentIndex(LeftHand.BoneIndex);
	if(LeftLowerArmIndex != INDEX_NONE) LeftUpperArmIndex = RefSkel.GetParentIndex(LeftLowerArmIndex);

	if(RightUpperArmIndex != INDEX_NONE) CachedRightUpperArmParentBoneIndex = RefSkel.GetParentIndex(RightUpperArmIndex);
	if(LeftUpperArmIndex != INDEX_NONE) CachedLeftUpperArmParentBoneIndex = RefSkel.GetParentIndex(LeftUpperArmIndex);
}

void FAnimNode_FPSArmsIK::CacheBones_AnyThread(const FAnimationCacheBonesContext& Context)
{
	BasePose.CacheBones(Context);
}

void FAnimNode_FPSArmsIK::GatherDebugData(FNodeDebugData& DebugData)
{
	BasePose.GatherDebugData(DebugData);
}

void FAnimNode_FPSArmsIK::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	BasePose.Update(Context);
	GetEvaluateGraphExposedInputs().Execute(Context);
}

bool FAnimNode_FPSArmsIK::CanEvaluate() const
{
	if(FMath::IsNearlyZero(Alpha))
		return false;
	
	if(!RightHand.IsValidToEvaluate() || !LeftHand.IsValidToEvaluate() || RightLowerArmIndex == INDEX_NONE || LeftLowerArmIndex == INDEX_NONE ||
		RightUpperArmIndex == INDEX_NONE || LeftUpperArmIndex == INDEX_NONE)
	{
		UE_LOG(LogTemp, Error, TEXT("True FPS Rig: Not all arm bones are valid"));
		return false;
	}

	if(CachedRightUpperArmParentBoneIndex == INDEX_NONE)
	{
		UE_LOG(LogTemp, Error, TEXT("True FPS Rig: Right Upper Arm does not have valid parent bone"));
		return false;
	}

	if(CachedLeftUpperArmParentBoneIndex == INDEX_NONE)
	{
		UE_LOG(LogTemp, Error, TEXT("True FPS Righ: Left Upper Arm does not have valid parent bone"));
		return false;
	}
			
	return true;		
}

#define CS_TRANSFORM(Index) FAnimationRuntime::GetComponentSpaceTransform(RefSkel, BSBoneTransforms, Index)

void FAnimNode_FPSArmsIK::Evaluate_AnyThread(FPoseContext& Output)
{
	BasePose.Evaluate(Output);
	if(!CanEvaluate()) return;
	
	CameraRelativeRotation.Normalize();
	
	//
	//	Cache initial transforms
	//

	const FBoneContainer& RequiredBones = Output.Pose.GetBoneContainer();
	const FReferenceSkeleton& RefSkel = RequiredBones.GetReferenceSkeleton();
	TArray<FTransform>& BSBoneTransforms = *(TArray<FTransform>*)&Output.Pose.GetBones();

	// Init arm transforms. Component-space.
	const FTransform CSInitRightJointTransform = CS_TRANSFORM(RightLowerArmIndex);
	const FTransform CSInitLeftJointTransform = CS_TRANSFORM(LeftLowerArmIndex);

	const FTransform CSInitRightHandTransform = BSBoneTransforms[RightHand.BoneIndex] * CSInitRightJointTransform;
	const FTransform CSInitLeftHandTransform = BSBoneTransforms[LeftHand.BoneIndex] * CSInitLeftJointTransform;
	
	const FTransform CSInitCameraTransform = FTransform(FRotator(0.f, MeshYawOffset, 0.f), (FTransform(CameraRelativeLocation) * CS_TRANSFORM(Head.BoneIndex)).GetLocation());
	FTransform CSInitWeaponTransform = OriginRelativeTransform * (bRightHanded ? CSInitRightHandTransform : CSInitLeftHandTransform);// Weapon relative transform is bone-space off of primary hand

	//
	//	Post aim offset applied
	//

	// Initialize arm transform vars
	FTransform RightUpperArmTransform = CS_TRANSFORM(RightUpperArmIndex);
	FTransform RightLowerArmTransform = BSBoneTransforms[RightLowerArmIndex] * RightUpperArmTransform;
	FTransform RightHandTransform = BSBoneTransforms[RightHand.BoneIndex] * RightLowerArmTransform;

	FTransform LeftUpperArmTransform = CS_TRANSFORM(LeftUpperArmIndex);
	FTransform LeftLowerArmTransform = BSBoneTransforms[LeftLowerArmIndex] * LeftUpperArmTransform;
	FTransform LeftHandTransform = BSBoneTransforms[LeftHand.BoneIndex] * LeftLowerArmTransform;

	//
	//	Calculate weapon transform in component-space
	//
	
	// Current component-space camera transform.
	const FTransform& CSCameraTransform = FTransform(CameraRelativeRotation + FRotator(0.f, MeshYawOffset, 0.f), (FTransform(CameraRelativeLocation) * CS_TRANSFORM(Head.BoneIndex)).GetLocation());

	// Camera transform to weapon transform with root offset and accumulative spine offset applied.
	const FTransform& CameraToWeaponTransform = CSInitWeaponTransform.GetRelativeTransform(
		FTransform(BSBoneTransforms[0].GetRotation()/* * AccumulativeOffsetInverse.Inverse()*/ * CSInitCameraTransform.GetRotation(), CSInitCameraTransform.GetLocation()));
	
	// Get current weapon transform by the camera offset
	FTransform CSWeaponTransform = CameraToWeaponTransform * CSCameraTransform;
	
	// Get aiming and non-aiming transforms
	const FTransform& AimingTransform = OriginRelativeTransform.GetRelativeTransform(SightsRelativeTransform) * CSCameraTransform;
	
	FTransform NonAimingTransform = CustomWeaponOffsetTransform * CSWeaponTransform;
	NonAimingTransform.SetRotation(FQuat::FastLerp(CSInitWeaponTransform.GetRotation(), NonAimingTransform.GetRotation(), WeaponRotationAlpha));
	NonAimingTransform.SetLocation(NonAimingTransform.GetLocation() * WeaponLocationAlpha + CSInitWeaponTransform.GetLocation() * (1.f - WeaponLocationAlpha));

	CSWeaponTransform = NonAimingTransform;
	CSWeaponTransform.NormalizeRotation();
	CSWeaponTransform.BlendWith(AimingTransform, AimingValue);
	CSWeaponTransform = OffsetTransform * CSWeaponTransform;

	// If arm pull-back is enabled...
	if(ArmPullbackConfig.Config == EArmPullbackConfig::Enabled || (ArmPullbackConfig.Config == EArmPullbackConfig::AimingValue && ArmPullbackConfig.ArmPullbackThreshold > AimingValue))
	{
		// Get the max reach and current reach amount on the non-dominant arm.
		// Then we will pull the weapon back by the reach over the threshold amount.
		float ReachDist;
		float MaxReach;
		if(bRightHanded)
		{// Calculate the projected hand transform
			ReachDist = (LeftUpperArmTransform.GetLocation() - (CSInitLeftHandTransform.GetRelativeTransform(CSInitWeaponTransform) * CSWeaponTransform).GetLocation()).Size();
			MaxReach = BSBoneTransforms[LeftLowerArmIndex].GetLocation().Size() + BSBoneTransforms[LeftHand.BoneIndex].GetLocation().Size();
		}
		else
		{
			ReachDist = (RightUpperArmTransform.GetLocation() - (CSInitRightHandTransform.GetRelativeTransform(CSInitWeaponTransform) * CSWeaponTransform).GetLocation()).Size();
			MaxReach = BSBoneTransforms[RightLowerArmIndex].GetLocation().Size() + BSBoneTransforms[RightHand.BoneIndex].GetLocation().Size();
		}

		// Calculate the distance to pull the weapon back
		const float PullbackDist = MaxReach * MaxExtension - ReachDist;
		if(PullbackDist < 0)// Pull back directly towards camera
		{
			const FTransform PullbackTransform((CSWeaponTransform.GetLocation() - CSCameraTransform.GetLocation()).GetSafeNormal() * PullbackDist);
			CSWeaponTransform *= PullbackTransform;
			if(!FMath::IsNearlyZero(AimingValue))// Offset non-aiming transform as well to calculate joint transform influence later.
				NonAimingTransform *= PullbackTransform;
		}
	}
	
	
	FTransform JointInfluenceWeaponTransform = CSWeaponTransform;
	if(!FMath::IsNearlyZero(AimingJointInfluence))// Blend joint influence transform by non-aiming transform by blend amount
		JointInfluenceWeaponTransform.BlendWith(NonAimingTransform, 1.f - AimingJointInfluence);

	//
	// Right Arm IK
	//

	// The new right hand transform in component-space
	const FTransform& RightEffectorTransform = CSInitRightHandTransform.GetRelativeTransform(CSInitWeaponTransform) * (RightHandAdditiveTransform * CSWeaponTransform);

	// Removing the custom weapon offset from the weapon transform to get the original joint location without an offset then applying whatever offset specified
	
	FTransform RightJointAdditiveTransform;
	RightJointAdditiveTransform.BlendWith(RightHandAdditiveTransform, RightHandAdditiveJointInfluence);

	// The target transform for the right joint in component-space
	const FTransform RightJointTargetTransform = RightJointAdditiveTransform * CSInitRightJointTransform.GetRelativeTransform(CSInitWeaponTransform) *
		FTransform(RightJointLocationOffset) * CustomWeaponOffsetTransform.Inverse() * JointInfluenceWeaponTransform;

	// Right joint location in component-space
	FVector RightJointLocation;
	if(RightJointClamp.IsClamping() && !FMath::IsNearlyZero(ArmsJointAlpha))
	{
		// The joint transform without any additives applied
		const FTransform JointNoAdditiveTransform = RightJointAdditiveTransform * CSInitRightJointTransform.GetRelativeTransform(CSInitWeaponTransform) *
			FTransform(RightJointLocationOffset) * CameraToWeaponTransform * CSCameraTransform;// CameraToWeaponTransform * CSCameraTransform gets the weapon transform without additives

		FTransform Offset = RightJointTargetTransform.GetRelativeTransform(JointNoAdditiveTransform);
		const FQuat Orientation = CSInitRightJointTransform.GetRotation();
		FVector OrientationOffsetLocation = Orientation.UnrotateVector(Offset.GetLocation());

		// Clamp the orientation location offset between the ranges
		ClampRange(OrientationOffsetLocation.Y, RightJointClamp.HorizontalRange);
		ClampRange(OrientationOffsetLocation.Z, RightJointClamp.VerticalRange);
		Offset.SetLocation(Orientation.RotateVector(OrientationOffsetLocation));
		
		// Set joint location by the clamped joint location blended by the target joint location
		RightJointLocation = (Offset * JointNoAdditiveTransform).GetLocation() * ArmsJointAlpha + RightJointTargetTransform.GetLocation() * (1.f - ArmsJointAlpha);
	}
	else
	{// Set to target location if no clamping is needed
		RightJointLocation = RightJointTargetTransform.GetLocation();
	}
	
	AnimationCore::SolveTwoBoneIK(RightUpperArmTransform, RightLowerArmTransform, RightHandTransform,
		RightJointLocation, RightEffectorTransform.GetLocation(), false, 0.f, 0.f);

	RightHandTransform.SetRotation(RightEffectorTransform.GetRotation());

	//
	// Left Arm IK
	//

	// The new left hand transform in component-space
	const FTransform LeftEffectorTransform = LeftHandAdditiveTransform * (CSInitLeftHandTransform.GetRelativeTransform(CSInitWeaponTransform) * CSWeaponTransform);

	FTransform LeftJointAdditiveTransform;
	LeftJointAdditiveTransform.BlendWith(LeftHandAdditiveTransform, LeftHandAdditiveJointInfluence);

	// Target transform for the left joint in component-space
	const FTransform LeftJointTargetTransform = LeftJointAdditiveTransform * CSInitLeftJointTransform.GetRelativeTransform(CSInitWeaponTransform) *
			FTransform(LeftJointLocationOffset) * CustomWeaponOffsetTransform.Inverse() * JointInfluenceWeaponTransform;

	// Left joint location in component-space
	FVector LeftJointLocation;
	if(LeftJointClamp.IsClamping() && !FMath::IsNearlyZero(ArmsJointAlpha))
	{
		// The joint transform without any additives applied
		const FTransform JointNoAdditiveTransform = LeftJointAdditiveTransform * CSInitLeftJointTransform.GetRelativeTransform(CSInitWeaponTransform) *
			FTransform(LeftJointLocationOffset) * CameraToWeaponTransform * CSCameraTransform;// CameraToWeaponTransform * CSCameraTransform gets the weapon transform without additives

		FTransform Offset = LeftJointTargetTransform.GetRelativeTransform(JointNoAdditiveTransform);
		const FQuat Orientation = CSInitLeftJointTransform.GetRotation();
		FVector OrientationOffsetLocation = Orientation.UnrotateVector(Offset.GetLocation());

		// Clamp the orientation location offset between the ranges
		ClampRange(OrientationOffsetLocation.Y, LeftJointClamp.HorizontalRange);
		ClampRange(OrientationOffsetLocation.Z, LeftJointClamp.VerticalRange);
		Offset.SetLocation(Orientation.RotateVector(OrientationOffsetLocation));

		// Set joint location by the clamped joint location blended by the target joint location
		LeftJointLocation = (Offset * JointNoAdditiveTransform).GetLocation() * ArmsJointAlpha + LeftJointTargetTransform.GetLocation() * (1.f - ArmsJointAlpha);
	}
	else
	{// Set to target location if no clamping is needed
		LeftJointLocation = LeftJointTargetTransform.GetLocation();
	}
	
	
	AnimationCore::SolveTwoBoneIK(LeftUpperArmTransform, LeftLowerArmTransform, LeftHandTransform,
		LeftJointLocation, LeftEffectorTransform.GetLocation(), false, 0.f, 0.f);

	LeftHandTransform.SetRotation(LeftEffectorTransform.GetRotation());
	
	//
	// Apply IKs
	//

	const float TotalArmsAlpha = Alpha * ArmsAlpha;
	BSBoneTransforms[RightUpperArmIndex].BlendWith(RightUpperArmTransform.GetRelativeTransform(CS_TRANSFORM(CachedRightUpperArmParentBoneIndex)), TotalArmsAlpha);
	BSBoneTransforms[RightLowerArmIndex].BlendWith(RightLowerArmTransform.GetRelativeTransform(RightUpperArmTransform), TotalArmsAlpha);
	BSBoneTransforms[RightHand.BoneIndex].BlendWith(RightHandTransform.GetRelativeTransform(RightLowerArmTransform), TotalArmsAlpha);

	BSBoneTransforms[LeftUpperArmIndex].BlendWith(LeftUpperArmTransform.GetRelativeTransform(CS_TRANSFORM(CachedLeftUpperArmParentBoneIndex)), TotalArmsAlpha);
	BSBoneTransforms[LeftLowerArmIndex].BlendWith(LeftLowerArmTransform.GetRelativeTransform(LeftUpperArmTransform), TotalArmsAlpha);
	BSBoneTransforms[LeftHand.BoneIndex].BlendWith(LeftHandTransform.GetRelativeTransform(LeftLowerArmTransform), TotalArmsAlpha);
}

#undef CS_TRANSFORM
